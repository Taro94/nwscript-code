# NWScript Code

This is basically a custom config for Visual Studio Code containing tasks, shortcuts and extensions to turn VSC into a pseudo-IDE for coding in NWScript (for Neverwinter Nights: Enhanced Edition).

## Where can I get it?

You can get it for Arch-based Linux distributions from [AUR](https://aur.archlinux.org/packages/nwscript-code).

## Where *else* can I get it?

I've assembled this config mainly for myself, which is why it is distributed only via AUR and not supported by me either on Windows or even other (non-arch) Linux distros. NWScript Code depends on a few pacman packages (official and from AUR) and it's too much hassle for me to create and maintain deb packages and/or Windows installers, sorry. :(

## Configuration file

After installation and launching NWScript Code once (with the `nwscript-code` command), close it and edit the `~/.local/nwscript-code/config.ini` file to set correct paths to your NWN directories and binaries (you can also optionally change the paths to NWScript Code's user directory and extensions directory). Don't end the paths with slashes, the config file doesn't like that at the moment. When you're done, save the file and you can re-launch the program.

## Features
- **Separate user directory from VSC**: NWScript Code maintains its own user directory and extensions, so you won't accidentally break your regular VSC setup with the NWScript setup or vice versa.
- **Pre-installed extensions**, including a modified glorwinger's nwscript syntax support (with the new sqlquery type recognition)
- **Pre-made tasks for a variety of NWN development actions** (with function key shortcuts), such as compilation, game log access, NWScript auto-formatting and more.
- **Integration with [nasher](https://github.com/squattingmonk/nasher)**
- **Integration with [NSSnippets](https://neverwintervault.org/project/nwn1/other/tool/nssnippets)** (custom NWScript snippets with functions and constants)
- **Integration with [NSSDefinitions](https://gitlab.com/Taro94/nwscript-definitions)** (a go-to-definition feature for NWScript functions)
- **Hot-reloading** (changing and saving a script with the game running will take immediate effect in the game)
- **Choosing test character** (if the .bic file specified in config.ini is found, that character will be used when testing the module with F9)
- **Support for the BioWare debugger** (if someone can get it to work at all... in which case I'd be happy to hear about it)

## Tasks and shortcuts

Tasks can be accessed via `Terminal->Run Task...` or by their respective function key shortcuts:
* **Hot-reload script (CTRL+S)** - if the game is running, saving a script with CTRL+S will compile that script (if it's a script with main) or all the scripts using it (if it's an include) and put them in the user directory's "development" folder to achieve hot-reload functionality; all the compiled scripts are deleted from "development" upon exiting the game; try to avoid hot-reloading libraries used by lots of scripts or be ready to wait a bit longer for the changes to take effect
* **Auto-format script (CTRL+K, CTRL+F)** - auto-format the current script
* **Go to definition/include (F2)** - goes to the definition of a selected function or constant symbol (if one of them is selected) or opens a script include file in the project (if a script include name is selected)
* **Reload snippets (F4)** - reloads custom snippets (autocomplete of functions, constants and include files) for the currently opened script; only those functions and constants with a header/signature declared in the script or one of its first-level include files will be suggested
* **Compile (F5)** - compiles the scripts in the project (via nasher, so only new or modified scripts will be compiled)
* **Pack module (F7)** - packs the module (first target defined in nasher.cfg)
* **Unpack module (F8)** - unpacks the module (first target defined in nasher.cfg)
* **Play module (SHIFT+F9)** - launches the game (after building and packing the module if necessary) and goes to the project module's character selection menu
* **Test module (F9)** - like above, but selects the first character on the list and starts the module; will choose a test character specified in config.ini if such a file exists
* **Debug module (SHIFT+F10)** - launches the game (after building and packing the module with debug symbols) with the debugger and goes to the project module's character selection menu; requires a target in the project's nasher.cfg file named "debug" which packs the module with debug symbols (.*ndb files)
* **Fast debug module (F10)** - like above, but selects the first character on the list (or character specified in config.ini) and starts the module
* **Client log (F11)** - opens the game's log file in a new tab
* **Open module in toolset (F12)** - packs the module (first target defined in nasher.cfg), opens it in Aurora for non-scripting tasks and unpacks it upon closing the toolset
