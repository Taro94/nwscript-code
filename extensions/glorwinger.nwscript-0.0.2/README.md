# README - nwscript syntax

Syntax higlighting for nwscript

## Features

This is basic nwscript highlighting based on the syntax highlighter for sublime text.

The nwscript syntax is used by Neverwinter Nights 1, Neverwinter Nights 2, and Neverwinter Nights Enhanced Edition

**Enjoy!**